require 'test_helper'

class JokeDbsControllerTest < ActionController::TestCase
  setup do
    @joke_db = joke_dbs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:joke_dbs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create joke_db" do
    assert_difference('JokeDb.count') do
      post :create, joke_db: { joke_content: @joke_db.joke_content }
    end

    assert_redirected_to joke_db_path(assigns(:joke_db))
  end

  test "should show joke_db" do
    get :show, id: @joke_db
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @joke_db
    assert_response :success
  end

  test "should update joke_db" do
    patch :update, id: @joke_db, joke_db: { joke_content: @joke_db.joke_content }
    assert_redirected_to joke_db_path(assigns(:joke_db))
  end

  test "should destroy joke_db" do
    assert_difference('JokeDb.count', -1) do
      delete :destroy, id: @joke_db
    end

    assert_redirected_to joke_dbs_path
  end
end
