class Joke < ActiveRecord::Base
	def self.daily_joke(options)
		if options.nil?
			joke_ids = self.ids
			joke = self.find(joke_ids.sample)
		else
			array = options.split(",")
			joke_ids = self.where('id NOT IN (?)',array).ids
		 	joke = joke_ids.blank? ? nil : self.find(joke_ids.sample)
		end
		joke
	end
	
end
