jQuery(document).ready(function($) {
	load_daily_joke();
	$(".btn-vote").click(function(event) {
		load_daily_joke();
	});
});

function load_daily_joke (argument) {
	$.ajax({
		url: '/jokes/daily_joke',
		type: 'GET',
		dataType: 'json',
	})
	.done(function(result) {
		$("#joke_content").html(result.message)
		console.log("success");
	})
	.fail(function() {
		alert("Can't connect to server")
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
}